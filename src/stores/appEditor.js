import { defineStore } from "pinia";
import request from "../services/api";
import transliteration from "ru-en-transliteration";

export const useAppEditorStore = defineStore({
  id: "appEditor",
  state: () => ({
    form: {
      name: null,
      slug: null,
      description: null,
      gitlab_project_id: null
    },
    id: null,
    isRecentlySaved: false,
    isLoading: false,
    errors: null
  }),
  actions: {
    setValue(key, value) {
      this.form[key] = value;
      if (key === 'name') {
        this.form.slug = transliteration.fromRuToEn(value)
                    .replace(new RegExp(' ', 'g'), '-')
                    .toLowerCase();
      }
    },
    async submit() {
      this.errors = null;
      this.isLoading = true;
      try {
        await request.post("/api/v1.0/apps/", this.form, {
          "Content-Type": "application/json"
        });
        this.isRecentlySaved = true;
        setTimeout(() => this.isRecentlySaved = false, 5000);
      } catch (e) {
        console.error(e);
      }
      this.isLoading = false;
    }
  }
});
