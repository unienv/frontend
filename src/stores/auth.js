import { defineStore } from "pinia";
import { retrieveProfile, setToken } from "../services/auth";

export const useAuthStore = defineStore({
  id: "auth",
  state: () => ({
    isLoading: true,
    error: null,
    profile: null
  }),
  actions: {
    async loadProfile() {
      this.isLoading = true;
      try {
        this.profile = await retrieveProfile();
      } catch (e) {
        console.log(e);
        if (e.response.status !== 401) {
          console.log(e);
        }
      }
      this.isLoading = false;
    },
    displayErrors(error) {
      if (error) {
        console.error(error);
        this.error = error === "access_denied"
          ? "Доступ к аккаунту запрещен, попробуйте войти еще раз"
          : "Произошла неизвестная ошибка, попробуйте еще раз";
      }
    },
    finishOAuth(token) {
      console.log(token);
      if (token !== undefined && token !== null) {
        try {
          setToken(token);
          location.search = '';
        } catch (e) {
          this.error = "Произошла неизвестная ошибка, попробуй еще раз";
          console.error(e);
        }
      }
    }
  },
  getters: {
    isAuthorized() {
      return this.profile !== null;
    }
  }
});
