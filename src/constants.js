export const UNIENV_CLIENT_ID = import.meta.env.VITE_UNIENV_CLIENT_ID;
export const UNIENV_HOST = import.meta.env.VITE_UNIENV_HOST || 'https://uenv-core.kpfu.ru';

export const FRONTEND_FOR_BACKEND_HOST = 'http://localhost:3001';
export const SENTRY_DSN = import.meta.env.VITE_SENTRY_DSN;
