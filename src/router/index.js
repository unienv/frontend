import { createRouter, createWebHistory } from 'vue-router'
import MainView from '../views/Main.vue'
import { useAuthStore } from "../stores/auth";
import { hasAccessToRoute } from "../services/auth";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'main',
      component: MainView
    },
    {
      path: '/apps',
      name: 'apps',
      component: () => import('../views/Apps.vue')
    },
    {
      path: '/apps/create',
      name: 'apps-create',
      component: () => import('../views/AppCreate.vue')
    },
    {
      path: '/apps/:id',
      name: 'app',
      component: () => import('../views/App.vue'),
      children: [
        {
          path: '',
          name: 'app-index',
          component: () => import('../views/AppIndex.vue')
        },
        {
          path: 'auth',
          name: 'app-auth',
          component: () => import('../views/AppAuth.vue')
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
    const authStore = useAuthStore();
    if (!hasAccessToRoute(authStore.isAuthorized, to)) {
        next({name: 'main'});
    }
    else {
        next();
    }
})

export default router
