import axios from "axios";
import { UNIENV_HOST } from "../constants";
import { getToken } from "./auth";
const token = getToken();

const request = axios.create({
  baseURL: UNIENV_HOST,
  headers: {
    Authorization: `Bearer ${token}`
  }
});

export default request;
