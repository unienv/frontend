import { FRONTEND_FOR_BACKEND_HOST, UNIENV_CLIENT_ID, UNIENV_HOST } from "../constants";
import request from "./api";

const CALLBACK_URL = `${FRONTEND_FOR_BACKEND_HOST}/oauth_callback`;

export function getAuthUrl() {
  return `${UNIENV_HOST}/oauth/authorize?response_type=code&client_id=${UNIENV_CLIENT_ID}&redirect_uri=${CALLBACK_URL}&scope=read gitlab`;
}

export function setToken(token) {
  localStorage.setItem('token', token);
}

export function getToken() {
  return localStorage.getItem('token');
}

export async function retrieveProfile() {
  const response = await request.get("/api/v1.0/profile/");
  return response.data;
}

export function hasAccessToRoute(isAuth, route) {
  if (isAuth) {
    return true;
  }
  return route.name === 'main';
}
