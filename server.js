const express = require("express");
const axios = require("axios");
const app = express();
require("dotenv").config();

const FRONTEND_HOST = process.env.FRONTEND_HOST || "http://localhost:3000";
const UNIENV_CLIENT_SECRET = process.env.VITE_UNIENV_CLIENT_SECRET;
const UNIENV_HOST = process.env.VITE_UNIENV_HOST || 'https://uenv-core.kpfu.ru';
const UNIENV_CLIENT_ID = process.env.VITE_UNIENV_CLIENT_ID;
const HOST = process.env.HOST || 'http://localhost:3001';
const CALLBACK_URL = `${HOST}/oauth_callback`;

app.use(express.json()); // for parsing application/json

app.get("/", function(req, res) {
  res.send("hello world");
});

app.post("/", function(req, res) {
  console.log(req.body);
  res.json({ "status": "ok" });
});

app.get("/oauth_callback", async function(req, res) {
  const code = req.query.code;
  if (!code) {
    res.redirect(FRONTEND_HOST);
    return;
  }

  const params = new URLSearchParams();
  params.append('client_id', UNIENV_CLIENT_ID);
  params.append('client_secret', UNIENV_CLIENT_SECRET);
  params.append('grant_type', 'authorization_code');
  params.append('code', code);
  params.append('redirect_uri', CALLBACK_URL);
  const response = await axios.post(`${UNIENV_HOST}/oauth/token/`, params);
  const token = response.data.access_token;
  res.redirect(`${FRONTEND_HOST}?token=${token}`);
});

const port = process.env.PORT || 3001;
app.listen(port);
console.log("server is listening port", port);

