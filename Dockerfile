FROM node:17-alpine as node

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY . .

ENV NODE_ENV production
RUN npm run build

FROM nginx:1.21-alpine

COPY --from=node /app/dist /app/dist
COPY host.conf /etc/nginx/conf.d/default.conf
